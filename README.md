# Test restFramework en django
Esta app es un test del framworkRest para python, se realizara los 4 metodos http
Get, Post, Put y delete con un ejemplo basico y conexion al a base de datos en postgres
## creación del proyecto 

- en el direcotrio instalar django
- crear el porjecto mediante ```django-admin startproject restfull```
- crear al app  ``` python manage.py startapp superheroes``` 
- instalar djangorestframework  ```pip install djangorestframewok```
- agregar al installedapps en el archivo settings.py ```'rest_framework',```
- crear los modelos y ejecutar las migrar ``` python manage.py makemigrations superheroes```

# Integración de OAUTH2
- instalar el paquete ```pip3 install django-oauth-toolkit```.
- agregar a intalledapps ``` 'oauth2_provider' ```.
- Le informamos a django rest framework que use el nuevo backend e autentificación en el settings.py con.

    ```
    REST_FRAMEWORK = {    
        'DEFAULT_AUTHENTICATION_CLASSES': (        
            'oauth2_provider.contrib.rest_framework.OAuth2Authentication',            
        )
    }
    ```