from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

urlpatterns = [
    # ex: /superheroe/
    path('superheroes/', views.SuperHeroeList.as_view()),
    # ex: /superheroe/5/
    path('<int:superhombres_id>/', views.SuperHeroeDetail.as_view()),
    # ex: /publishers/
    path('publishers', views.PublisherList.as_view()),
    # ex: /publisher/5/
    path('<int:publisher_id>/', views.SuperHeroeDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)