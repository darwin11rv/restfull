from rest_framework import serializers
from .models import Superhombres, Publisher


class SuperhombresSerializer(serializers.ModelSerializer):
    class Meta:
        model = Superhombres
        fields = ('id', 'name', 'gender', 'real_name', 'publisher')

class PublisherSerializer(serializers.ModelSerializer):
    class Meta:
        model = Publisher
        fields = ('id', 'name', 'founder')