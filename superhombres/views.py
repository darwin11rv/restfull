from django.shortcuts import render

from .models import Superhombres, Publisher
from .serializers import SuperhombresSerializer, PublisherSerializer
from rest_framework import generics

# Create your views here.

#para los metodos get y post
class SuperHeroeList(generics.ListCreateAPIView):
    queryset = Superhombres.objects.all()
    serializer_class = SuperhombresSerializer

#para los metodos get patch delete
class SuperHeroeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Superhombres.objects.all()
    serializer_class = SuperhombresSerializer

#para los metodos get y post
class PublisherList(generics.ListCreateAPIView):
    queryset = Publisher.objects.all()
    serializer_class = PublisherSerializer

#para los metodos get patch delete
class PublisherDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Publisher.objects.all()
    serializer_class = PublisherSerializer